package pro.oncor.simplemap.entities

/**
 * Сущность юзера
 */
class EntityOperator(val id: Int, val fio: String, val passw: String)