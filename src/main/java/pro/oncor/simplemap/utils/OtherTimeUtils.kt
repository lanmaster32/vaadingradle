package pro.oncor.simplemap.utils

import org.apache.commons.lang3.time.DurationFormatUtils
import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Конвертеры для даты и времени
 */
object OtherTimeUtils {
    fun timestampToStringFull(timestamp: Timestamp): String {
        return if (Objects.nonNull(timestamp)) timestamp.toLocalDateTime()
            .format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")) else ""
    }

    fun timestampToStringDate(timestamp: Timestamp): String {
        return if (Objects.nonNull(timestamp)) timestamp.toLocalDateTime()
            .format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) else ""
    }

    fun timestampToStringHHMM(timestamp: Timestamp): String {
        return if (Objects.nonNull(timestamp)) timestamp.toLocalDateTime()
            .format(DateTimeFormatter.ofPattern("HH:mm")) else ""
    }

    fun currentDateTimeToStringFullFilenameFriendly(): String {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_HH-mm-ss"))
    }

    @JvmStatic
    fun currentDateTimeToStringFull(): String {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"))
    }

    /**
     * Строка длительности с миллисекундами
     */
    fun durationStringDHMSs(millis: Long?): String {
        return DurationFormatUtils.formatDuration(millis!!, "d'd' H'h' m'm' s.SSS's'")
    }

    /**
     * Строка длительности
     */
    fun durationStringDHHMMSS(millis: Long?): String {
        return DurationFormatUtils.formatDuration(millis!!, "d'd' HH'h' mm'm' ss's'")
    }

    fun durationStringDHHMMSS(millis: Double): String {
        return DurationFormatUtils.formatDuration(millis.toLong(), "d'd' HH'h' mm'm' ss's'")
    }
}