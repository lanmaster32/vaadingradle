package pro.oncor.simplemap.persistentconfiguration

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import jetbrains.exodus.entitystore.Entity
import jetbrains.exodus.entitystore.PersistentEntityStore
import jetbrains.exodus.entitystore.PersistentEntityStores
import jetbrains.exodus.entitystore.StoreTransaction
import jetbrains.exodus.env.EnvironmentConfig
import jetbrains.exodus.env.Environments.newInstance
import org.apache.commons.lang3.reflect.TypeUtils
import org.slf4j.LoggerFactory
import pro.oncor.simplemap.Application.registerRuntimeShutdownHook
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.map.SpatialEvent
import java.io.IOException
import java.util.*
import java.util.function.Consumer
import java.util.stream.Collectors
import java.util.stream.StreamSupport

/**
 * Сохранение / загрузка сущностей
 */
object PersistentConfiguration {
    private const val ENTITY_CLASS_NAME = "SpatialEvent"
    private val LOGGER = LoggerFactory.getLogger(PersistentConfiguration::class.java)
    private val OBJECT_MAPPER = ObjectMapper()
    val ENTITY_STORE: PersistentEntityStore = PersistentEntityStores.newInstance(
        newInstance(
            "params/exodusdb",
            EnvironmentConfig().setGcEnabled(true).setLogFileSize(1024L)
        )
    )

    init {
        registerRuntimeShutdownHook("Завершение работы EXODUS DB") {
            ENTITY_STORE.close()
            LOGGER.info("Closing EXODUS DB before system shutdown...")
        }
    }

    @JvmStatic
    @Synchronized
    fun persist(spatialEvent: SpatialEvent) {
        ENTITY_STORE.executeInExclusiveTransaction { txn: StoreTransaction ->
            val entity = txn.newEntity(ENTITY_CLASS_NAME)
            eventToEntity(spatialEvent, entity)
            txn.saveEntity(entity)
            spatialEvent.id = entity.id.localId
        }
    }

    @JvmStatic
    fun merge(spatialEvent: SpatialEvent) {
        ENTITY_STORE.executeInExclusiveTransaction { txn: StoreTransaction ->
            val entities = txn.findIds(ENTITY_CLASS_NAME, spatialEvent.id, spatialEvent.id)
            var entity = entities.first
            if (Objects.nonNull(entity)) {
                eventToEntity(spatialEvent, entity)
                txn.saveEntity(entity!!)
            } else {
                entity = txn.newEntity(ENTITY_CLASS_NAME)
                eventToEntity(spatialEvent, entity)
                txn.saveEntity(entity)
                spatialEvent.id = entity.id.localId
            }
        }
    }

    @JvmStatic
    fun refresh(spatialEvent: SpatialEvent) {
        if (spatialEvent.id != -1L) {
            ENTITY_STORE.executeInExclusiveTransaction { txn: StoreTransaction ->
                val entities = txn.findIds(ENTITY_CLASS_NAME, spatialEvent.id, spatialEvent.id)
                val entity = entities.first
                if (Objects.nonNull(entity)) {
                    entityToEvent(entity, spatialEvent)
                }
            }
        }
    }

    @JvmStatic
    val allItems: List<SpatialEvent>
        get() {
            val collect: MutableList<SpatialEvent> = ArrayList()
            ENTITY_STORE.executeInExclusiveTransaction { txn: StoreTransaction ->
                val all = txn.getAll(ENTITY_CLASS_NAME)
                val events = StreamSupport.stream(all.spliterator(), false)
                    .map { entity: Entity ->
                        val spatialEvent = SpatialEvent()
                        spatialEvent.id = entity.id.localId
                        entityToEvent(entity, spatialEvent)
                        spatialEvent
                    }
                    .collect(Collectors.toList())
                collect.addAll(events)
            }
            return collect
        }

    /**
     * Обновление записи в базе из сущности
     */
    private fun eventToEntity(event: SpatialEvent, entity: Entity?) {
        entity!!.setProperty("title", event.title)
        entity.setProperty("date", event.date.time)
        entity.setProperty("coordinate", toJson(event.location.coordinate))
    }

    /**
     * Обновление полей сущности из записи в базе
     */
    private fun entityToEvent(entity: Entity?, spatialEvent: SpatialEvent) {
        val title = convert(entity!!.getProperty("title"), String::class.java, "")!!
        val aLong = convert(entity.getProperty("date"), Long::class.java, 0L)!!
        val coordinate = convert(entity.getProperty("coordinate"), String::class.java, "")!!
        spatialEvent.title = title
        spatialEvent.date = Date(aLong)
        spatialEvent.location = GeometryFactory().createPoint(fromJson(coordinate))
    }

    @JvmStatic
    fun remove(spatialEvent: SpatialEvent) {
        ENTITY_STORE.executeInExclusiveTransaction { txn: StoreTransaction ->
            val entities = txn.findIds(ENTITY_CLASS_NAME, spatialEvent.id, spatialEvent.id)
            entities.forEach(Consumer { obj: Entity -> obj.delete() })
        }
    }

    private fun <T> convert(title: Comparable<*>?, tClass: Class<T>, defaultValue: T): T? {
        if (Objects.nonNull(title) && TypeUtils.isInstance(
                title,
                tClass
            )
        ) return title as T? else LOGGER.error("Cannot convert from Comparable to {}", tClass)
        return defaultValue
    }

    private fun fromJson(json: String): Coordinate {
        try {
            return OBJECT_MAPPER.readValue(json, Coordinate::class.java)
        } catch (e: IOException) {
            LOGGER.error("Error creating Object from Json String", e)
        }
        return Coordinate(0.0, 0.0)
    }

    private fun toJson(o: Coordinate): String {
        try {
            return OBJECT_MAPPER.writeValueAsString(o)
        } catch (e: JsonProcessingException) {
            LOGGER.error("Error creating Json String from Object", e)
        }
        return "{}"
    }
}