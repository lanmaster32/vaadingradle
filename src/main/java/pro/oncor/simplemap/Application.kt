package pro.oncor.simplemap

import ch.qos.logback.core.joran.spi.JoranException
import ch.qos.logback.ext.spring.LogbackConfigurer
import org.slf4j.LoggerFactory
import org.springframework.context.support.GenericXmlApplicationContext
import java.io.FileNotFoundException

/**
 * Minimal Servlet bootstrap for Vaadin application.
 */
object Application {

    private val LOGGER = LoggerFactory.getLogger(Application::class.java)

    // Контекст SPRING
    val applicationContext = GenericXmlApplicationContext()
    @JvmStatic
    fun main(args: Array<String>) {
        prepareLogback()
        applicationContext.load("file:cfg/spring/application.xml")
        applicationContext.refresh()
    }

    /**
     * Подготовка логгера LOGBACK для работы
     */
    private fun prepareLogback() {
        try {
            LogbackConfigurer.initLogging("file:cfg/logback.xml")
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: JoranException) {
            e.printStackTrace()
        }
    }

    /**
     * Регистрация операции, вызываемой при завершении приложения
     * [][<a href=]//www.baeldung.com/jvm-shutdown-hooks">...">&lt;a href=&quot;https://www.baeldung.com/jvm-shutdown-hooks&quot;&gt;...&lt;/a&gt;
     * [][<a href=]//docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Runtime.html.addShutdownHook">&lt;a href=&quot;https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Runtime.html#addShutdownHook(java.lang.Thread)&quot;&gt;...&lt;/a&gt;
     *
     * @since 2021-09-19
     */
    @JvmStatic
    fun registerRuntimeShutdownHook(description: String?, shutdownOperation: Runnable) {
        val unstartedHookThread = Thread {
            shutdownOperation.run()
            LOGGER.info("Завершение приложения. Вызов операции '{}'", description)
        }
        Runtime.getRuntime().addShutdownHook(unstartedHookThread)
    }
}