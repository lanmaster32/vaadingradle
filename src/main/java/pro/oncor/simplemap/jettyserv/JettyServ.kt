package pro.oncor.simplemap.jettyserv

import org.eclipse.jetty.http.HttpScheme
import org.eclipse.jetty.server.*
import org.eclipse.jetty.server.handler.HandlerList
import org.eclipse.jetty.server.handler.HandlerWrapper
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.ReceiveOrderVaadinUI
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.VaadinServletWithListeners

/**
 * Запуск JETTY сервера
 */
class JettyServ : InitializingBean {
    var httpHost: String? = null
    var httpPort = 0
    var mainContext: String? = null
    override fun afterPropertiesSet() {

        // ServletContextHandler для основного сервлета
        val servletContextHandlerWebclient = ServletContextHandler(ServletContextHandler.SESSIONS)
        servletContextHandlerWebclient.contextPath = mainContext
        servletContextHandlerWebclient.setInitParameter("ui", ReceiveOrderVaadinUI::class.java.canonicalName)
        servletContextHandlerWebclient.addServlet(ServletHolder(VaadinServletWithListeners()), "/*")
        val handlers = HandlerList()
        handlers.handlers = arrayOf<Handler>(servletContextHandlerWebclient)
        val handlerWrapper = HandlerWrapper()
        handlerWrapper.handler = handlers
        val server = Server()
        server.handler = handlerWrapper

        // Подключение HTTP коннектора
        val httpConnector = createHTTPConnector(server)
        server.addConnector(httpConnector)
        logger.info("Коннектор HTTP подключен: {}", httpConnector)
        try {
            server.start()
        } catch (ex: Exception) {
            logger.error("Ошибка при запуске сервера Jetty", ex)
        }
    }

    /**
     * Создание коннектора для HTTP
     */
    private fun createHTTPConnector(server: Server): ServerConnector {
        // HTTP Configuration for HTTP
        val httpConfig = HttpConfiguration()
        httpConfig.secureScheme = HttpScheme.HTTP.asString()
        httpConfig.sendServerVersion = false
        httpConfig.sendDateHeader = false

        // SSL Connector
        val httpConnector = ServerConnector(
            server,
            HttpConnectionFactory(httpConfig)
        )
        httpConnector.host = httpHost
        httpConnector.port = httpPort
        return httpConnector
    }

    companion object {
        private val logger = LoggerFactory.getLogger(JettyServ::class.java)
    }
}