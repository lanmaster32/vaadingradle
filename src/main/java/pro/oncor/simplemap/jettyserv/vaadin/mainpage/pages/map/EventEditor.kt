package pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.map

import com.vaadin.data.fieldgroup.BeanFieldGroup
import com.vaadin.ui.*
import org.slf4j.LoggerFactory
import org.vaadin.addon.leaflet.util.AbstractJTSField
import org.vaadin.viritin.button.PrimaryButton
import org.vaadin.viritin.layouts.MVerticalLayout
import java.util.*

/**
 * Редактор
 * @param spatialEvent - сущность редактирования
 * @param afterOps     - финальные операции после изменения/создания сущности
 */
class EventEditor(private val spatialEvent: SpatialEvent, private val afterOps: Runnable) : Window(), Button.ClickListener {

    private val LOGGER = LoggerFactory.getLogger(EventEditor::class.java)
    private val save: Button = PrimaryButton("Save", this)
    private val cancel = Button("Cancel", this)
    private val title = TextField("Title")
    private val date = DateField("Date")

    /* Used for EventWithPoint, field used for bean binding */
    private val location: PointFieldCloser

    /* Used for EventWithRoute, field used for bean binding */
    private val geometryField: AbstractJTSField<*>

    init {
        location = PointFieldCloser()
        geometryField = location

        /* Configure the sub window editing the pojo */caption = "Edit event"
        setHeight("80%")
        setWidth("80%")
        isModal = true
        isClosable = false // only via save/cancel

        /* Build layout */
        val verticalLayout = MVerticalLayout(
            title, date, geometryField,
            HorizontalLayout(save, cancel)
        ).expand(geometryField)
        content = verticalLayout

        /* Bind data to fields */bindFields(spatialEvent)
        title.focus()
    }

    private fun bindFields(e: SpatialEvent) {
        /* Std. naming convention based Vaadin field binding. */
        BeanFieldGroup.bindFieldsUnbuffered(e, this)
    }

    override fun buttonClick(event: Button.ClickEvent) {
        if (event.button === save) {
            try {
                JPAUtil.saveOrPersist(spatialEvent)
                Optional.ofNullable(afterOps).ifPresent { obj: Runnable -> obj.run() }
            } catch (e: Exception) {
                LOGGER.info("JPA Exception", e)
                JPAUtil.refresh(spatialEvent)
            }
        } else {
            JPAUtil.refresh(spatialEvent)
        }
        close()
    }
}