package pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.map

import com.vaadin.addon.contextmenu.ContextMenu
import com.vaadin.addon.contextmenu.MenuItem
import com.vaadin.server.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.UI
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.geom.MultiLineString
import com.vividsolutions.jts.geom.PrecisionModel
import org.vaadin.addon.leaflet.*
import org.vaadin.addon.leaflet.shared.Point
import org.vaadin.addon.leaflet.util.JTSUtil
import org.vaadin.viritin.button.MButton
import org.vaadin.viritin.fields.MTable
import org.vaadin.viritin.fields.MTable.RowClickEvent
import org.vaadin.viritin.layouts.MHorizontalLayout
import pro.oncor.simplemap.persistentconfiguration.PersistentConfiguration
import java.util.*

class LeafletMap {

    private var lastContextMenuPosition: Point? = null
    var table: MTable<SpatialEvent>? = null
        private set
    var map: LMap? = null
        private set
    private val osmTiles: LTileLayer = LOpenStreetMapLayer()

    init {
        createMap()
        createTable()
        loadEvents()
    }

    private fun createMap() {
        map = LMap()
        val contextMenu = ContextMenu(map, false)
        contextMenu.addItem("Добавить точку") { e: MenuItem? ->
            val event = SpatialEvent()
            event.location = JTSUtil.toPoint(lastContextMenuPosition)
            // Открытие окна редактирования точки
            Optional.ofNullable(UI.getCurrent())
                .ifPresent { ui: UI -> ui.addWindow(EventEditor(event) { loadEvents() }) }
        }

        // Получение координат клика по карте при правом клике
        map!!.addContextMenuListener { event: LeafletContextMenuEvent ->
            lastContextMenuPosition = event.point
            contextMenu.open(event.clientX.toInt(), event.clientY.toInt())
        }
        osmTiles.attributionString = "© OpenStreetMap Contributors"
    }

    /**
     * Создание таблицы
     */
    private fun createTable() {
        table = MTable(SpatialEvent::class.java)
        table!!.setWidth("100%")
        table!!.withGeneratedColumn("Actions") { spatialEvent: SpatialEvent? ->
            val edit: Button = MButton(FontAwesome.EDIT) { e: Button.ClickEvent? ->
                Optional.ofNullable(UI.getCurrent())
                    .ifPresent { ui: UI -> ui.addWindow(EventEditor(spatialEvent!!) { loadEvents() }) }
            }
            val delete: Button = MButton(FontAwesome.TRASH) { e: Button.ClickEvent? ->
                JPAUtil.remove(spatialEvent)
                loadEvents()
            }
            MHorizontalLayout(edit, delete)
        }
        table!!.withProperties("id", "title", "date", "Actions")
        table!!.addRowClickListener { event: RowClickEvent<SpatialEvent> ->
            if (event.isDoubleClick) {
                Optional.ofNullable(UI.getCurrent())
                    .ifPresent { ui: UI -> ui.addWindow(EventEditor(event.entity) { loadEvents() }) }
            }
        }
    }

    private fun loadEvents() {
        if (PersistentConfiguration.allItems.isEmpty()) {
            val theEvent = SpatialEvent()
            theEvent.title = "БЦ Манеж"
            theEvent.date = Date()
            theEvent.location = GeometryFactory().createPoint(Coordinate(60.65486, 56.83660))
            PersistentConfiguration.persist(theEvent)
        }
        val events = JPAUtil.listAllSpatialEvents()


        // todo собрать полигон и вычислить площадь
//        val map1: List<com.vividsolutions.jts.geom.Point> = events.map { t -> t.location }
//        val map2: List<com.vividsolutions.jts.geom.Coordinate> = events.map { t -> t.location.coordinate }
//        val multiLineStrings: Array<MultiLineString> = GeometryFactory.toMultiLineStringArray(map1)
//        val geometryFactory = GeometryFactory(PrecisionModel(), 4326)
//        geometryFactory.createPolygon()




        // Populate table and map
        table!!.setBeans(events)
        map!!.removeAllComponents()
        map!!.addBaseLayer(L310Layer(), "OSM-T")
        map!!.addBaseLayer(osmTiles, "OSM")
        for (spatialEvent in events) {
            if (spatialEvent.location != null) {
                val layer = JTSUtil.toLayer(spatialEvent.location) as AbstractLeafletLayer
                // Add click listener to open event editor
                layer.addClickListener { event: LeafletClickEvent? ->
                    Optional.ofNullable(UI.getCurrent())
                        .ifPresent { ui: UI -> ui.addWindow(EventEditor(spatialEvent) { loadEvents() }) }
                }
                map!!.addLayer(layer)
            }
        }
        map!!.zoomToContent()
    }
}