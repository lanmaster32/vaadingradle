package pro.oncor.simplemap.jettyserv.vaadin.mainpage

import com.vaadin.server.*
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap
import javax.servlet.ServletException

class VaadinServletWithListeners : VaadinServlet() {
    @Throws(ServletException::class)
    override fun servletInitialized() {
        super.servletInitialized()

        // Инициализация сессии
        VaadinService.getCurrent().addSessionInitListener(SessionInitListener { event: SessionInitEvent ->
            val session = event.session
            //mapVaadinSessionToCodOp[session] = 0
            val remoteAddr = event.request.remoteAddr
            logger.info("Remote addr: {}. Session initialized: {}", remoteAddr, session)
        })

        // Уничтожение сессии
        VaadinService.getCurrent().addSessionDestroyListener(SessionDestroyListener { event: SessionDestroyEvent ->
            val session = event.session
            //mapVaadinSessionToCodOp.remove(session)
            logger.info("Session destroyed: {}", session)
        })
    }

    companion object {
        private val logger = LoggerFactory.getLogger(VaadinServletWithListeners::class.java)
    }
}