package pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.authenticatedwindow

import com.vaadin.server.Sizeable
import com.vaadin.ui.*
import com.vaadin.ui.themes.ValoTheme
import org.vaadin.viritin.layouts.MVerticalLayout
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.ReceiveOrderVaadinUI
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.SessionAttributes
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.authentication.Authentication
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.login.LoginWindow
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.map.LeafletMap
import pro.oncor.simplemap.utils.OtherTimeUtils.currentDateTimeToStringFull
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Окно для авторизованных юзеров
 */
class AuthenticatedUserWindow : VerticalLayout() {
    /**
     * Конструктор
     */
    init {
        setSizeFull()
        // Верхнее меню
        val topMenuPanel = createTopMenuPanel()
        addComponent(topMenuPanel)
        // Средняя часть
        val middleLayout = createMiddleLayout()
        addComponent(middleLayout)
        setExpandRatio(topMenuPanel, .5f)
        setExpandRatio(middleLayout, 10.0f)
    }

    /**
     * Верхнее горизонтальное меню
     */
    private fun createTopMenuPanel(): Component {
        val panel = Panel()
        panel.setWidth(100f, Sizeable.Unit.PERCENTAGE)
        panel.setHeight("200px")
        val horizontalLayout = HorizontalLayout()
        horizontalLayout.isSpacing = true
        horizontalLayout.setMargin(true)
        val labelUserName = Label(
            "Пользователь: " + ReceiveOrderVaadinUI.getSessionAttribute(SessionAttributes.USER_NAME).toString() + "  "
        )
        horizontalLayout.addComponent(labelUserName)
        horizontalLayout.setComponentAlignment(labelUserName, Alignment.MIDDLE_LEFT)
        val buttonLogout = createButtonLogout()
        horizontalLayout.addComponent(buttonLogout)
        horizontalLayout.setComponentAlignment(buttonLogout, Alignment.MIDDLE_LEFT)
        // Время сервера
        val labelTime = Label()
        horizontalLayout.addComponent(labelTime)
        horizontalLayout.setComponentAlignment(labelTime, Alignment.MIDDLE_RIGHT)
        createScheduler(labelTime)

        panel.content = horizontalLayout
        return panel
    }

    /**
     * Шедулер для обновления времени в шапке страницы
     */
    private fun createScheduler(labelTime: Label) {
        Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(
            {
                Optional.ofNullable(ui)
                    .ifPresent { ui: UI -> ui.access { labelTime.value = currentDateTimeToStringFull() } }
            },
            0, 1000, TimeUnit.MILLISECONDS
        )
    }

    /**
     * Средняя часть страницы
     */
    private fun createMiddleLayout(): Component {
        val leafletMap = LeafletMap()
        return MVerticalLayout().expand(leafletMap.map, leafletMap.table)
    }

    /**
     * Кнопка логаута
     */
    private fun createButtonLogout(): Component {
        val button = Button("Выход")
        button.addStyleName(ValoTheme.BUTTON_TINY)
        button.addClickListener { event: Button.ClickEvent? ->
            val authentication = ReceiveOrderVaadinUI.getSessionAttribute(
                SessionAttributes.AUTHENTICATION
            ) as Authentication
            authentication.logout()
            UI.getCurrent().content = LoginWindow()
        }
        return button
    }
}