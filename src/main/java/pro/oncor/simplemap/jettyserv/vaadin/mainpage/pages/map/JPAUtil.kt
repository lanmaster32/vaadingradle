package pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.map

import pro.oncor.simplemap.persistentconfiguration.PersistentConfiguration
import pro.oncor.simplemap.persistentconfiguration.PersistentConfiguration.allItems
import pro.oncor.simplemap.persistentconfiguration.PersistentConfiguration.merge
import pro.oncor.simplemap.persistentconfiguration.PersistentConfiguration.persist

object JPAUtil {

    fun saveOrPersist(entity: SpatialEvent) {
        if (entity.id == -1L) {
            persist(entity)
        } else {
            merge(entity)
        }
    }

    fun refresh(entity: SpatialEvent?) {
        PersistentConfiguration.refresh(entity!!)
    }

    fun listAllSpatialEvents(): List<SpatialEvent> {
        return allItems
    }

    fun remove(spatialEvent: SpatialEvent?) {
        PersistentConfiguration.remove(spatialEvent!!)
    }
}