package pro.oncor.simplemap.jettyserv.vaadin.mainpage

/**
 * Список идентификаторов, используемых в сессии Vaadin
 */
object SessionAttributes {
    // Название переменной, которая размещается в сесссии Vaadin и содержит в себе маппинг для
    // всех остальных сессионных переменных.
    // Не используется штатная система сессионных переменных, т.к. туда нельзя записать значение NULL
    var sam = "SESSION_ATTRIBUTE_MAPPER_DONT_USE_IT_MANUALLY!!!"

    // Имена переменных для хранения в сессии
    var AUTHENTICATION = "AUTHENTICATION"
    var COD_OP = "CodOp"
    var USER_NAME = "UserName"
}