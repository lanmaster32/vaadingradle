package pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.map

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import java.util.*

class SpatialEvent {
    var id = -1L
    var title = ""
    var date = Date()
    var location = GeometryFactory().createPoint(Coordinate(0.0, 0.0))
}