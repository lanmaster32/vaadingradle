package pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.map

import org.vaadin.addon.leaflet.util.PointField

class PointFieldCloser : PointField() {
    override fun prepareEditing() {
        super.prepareEditing()
        map.zoomLevel = 15.0
    }
}