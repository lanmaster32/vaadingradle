package pro.oncor.simplemap.jettyserv.vaadin.mainpage.authentication

import com.google.common.collect.ImmutableList
import com.vaadin.ui.UI
import pro.oncor.simplemap.entities.EntityOperator
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.ReceiveOrderVaadinUI
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.SessionAttributes
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.VaadinServletWithListeners

class Authentication {
    var isAuthenticated = false // Признак логина
        private set

    fun authenticate(codOp: Int, passw: String) {
        OPERATORS.stream()
            .filter { operator: EntityOperator -> operator.id == codOp && operator.passw == passw }
            .findFirst()
            .ifPresent { operator: EntityOperator -> login(codOp, operator.fio) }
    }

    private fun login(codOp: Int, fio: String) {
        ReceiveOrderVaadinUI.setSessionAttribute(SessionAttributes.COD_OP, codOp)
        ReceiveOrderVaadinUI.setSessionAttribute(SessionAttributes.USER_NAME, fio)
        isAuthenticated = true
    }

    fun logout() {

        // Закрываем все окна, открытые для текущего UI
        // https://vaadin.com/forum#!/thread/425126
        for (w in ArrayList(UI.getCurrent().windows)) {
            UI.getCurrent().removeWindow(w)
        }
        ReceiveOrderVaadinUI.setSessionAttribute(SessionAttributes.COD_OP, null)
        ReceiveOrderVaadinUI.setSessionAttribute(SessionAttributes.USER_NAME, null)
        isAuthenticated = false
    }

    companion object {
        @JvmField
        val OPERATORS: List<EntityOperator> = ArrayList(
            ImmutableList.of(
                EntityOperator(0, "SuperUser0", "demo"),
                EntityOperator(1, "SuperUser1", "demo"),
                EntityOperator(2, "SuperUser2", "demo"),
                EntityOperator(3, "SuperUser3", "demo"),
                EntityOperator(4, "SuperUser4", "demo")
            )
        )
    }
}