package pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.login

import com.vaadin.data.Property
import com.vaadin.data.util.IndexedContainer
import com.vaadin.event.ShortcutAction
import com.vaadin.server.FontAwesome
import com.vaadin.server.Sizeable
import com.vaadin.shared.ui.combobox.FilteringMode
import com.vaadin.ui.*
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.ReceiveOrderVaadinUI
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.SessionAttributes
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.authentication.Authentication
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.authenticatedwindow.AuthenticatedUserWindow
import java.util.*

/**
 * Created by alexander on 02.11.2016.
 */
class LoginWindow : VerticalLayout() {
    private var fioComboBox: ComboBox? = null
    private var passwordField: PasswordField? = null
    private var errorLabel: Label? = null
    private var sendButton: Button? = null

    init {
        val loginPanel = Panel("Авторизация")
        loginPanel.icon = FontAwesome.WINDOWS
        loginPanel.setSizeUndefined()
        loginPanel.setHeightUndefined()
        addComponent(loginPanel)
        createFioComboBox()
        createPasswordField()
        createErrorLabel()
        createButton()
        val formLayout = FormLayout()
        formLayout.addComponent(fioComboBox)
        formLayout.addComponent(passwordField)
        formLayout.addComponent(errorLabel)
        formLayout.addComponent(sendButton)
        formLayout.setSizeUndefined()
        formLayout.setMargin(true)
        loginPanel.content = formLayout
        loginPanel.setWidth(600f, Sizeable.Unit.PIXELS)
        setSizeFull()
        setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER)
        fioComboBox!!.focus()
    }

    private fun createButton() {
        sendButton = Button("Вход")
        sendButton!!.icon = FontAwesome.USER
        sendButton!!.setHeight("25px")

        sendButton!!.addClickListener { event: Button.ClickEvent? ->
            Optional.ofNullable(
                fioComboBox!!.value
            )
                .ifPresent { item: Any ->
                    if (passwordField!!.value.isEmpty()) {
                        passwordField!!.focus()
                        errorLabel!!.isVisible = false
                    } else {
                        val authentication =
                            ReceiveOrderVaadinUI.getSessionAttribute(SessionAttributes.AUTHENTICATION) as Authentication
                        authentication.authenticate(item as Int, passwordField!!.value)
                        if (authentication.isAuthenticated) {
                            //ReceiveOrderVaadinUI.getCurrent().content = AuthenticatedUserWindow()
                            UI.getCurrent().content = AuthenticatedUserWindow()
                        } else {
                            passwordField!!.clear()
                            passwordField!!.focus()
                            errorLabel!!.isVisible = true
                            errorLabel!!.value = "Ошибка авторизации"
                        }
                    }
                }
        }
        sendButton!!.setClickShortcut(ShortcutAction.KeyCode.ENTER)
        sendButton!!.description = "Кнопка ENTER"
    }

    private fun createErrorLabel() {
        errorLabel = Label()
        errorLabel!!.isVisible = false
    }

    private fun createPasswordField() {
        passwordField = PasswordField("Пароль")
        passwordField!!.setWidth(400f, Sizeable.Unit.PIXELS)
    }

    fun createFioComboBox() {
        fioComboBox = ComboBox("Пользователь", createContainerOperators())
        fioComboBox!!.pageLength = 10
        fioComboBox!!.inputPrompt = "Начните вводить свое имя (пароль: demo)"
        fioComboBox!!.itemCaptionPropertyId = COMBOBOX_CONTAINER_FIO
        fioComboBox!!.itemCaptionMode = AbstractSelect.ItemCaptionMode.PROPERTY
        fioComboBox!!.setWidth("400px")
        fioComboBox!!.filteringMode = FilteringMode.CONTAINS
        fioComboBox!!.isImmediate = true
        fioComboBox!!.isNullSelectionAllowed = false
        fioComboBox!!.addValueChangeListener { event: Property.ValueChangeEvent? -> passwordField!!.focus() }
    }

    private fun createContainerOperators(): IndexedContainer {
        val container = IndexedContainer()
        container.addContainerProperty(COMBOBOX_CONTAINER_FIO, String::class.java, null)
        container.addContainerProperty(COMBOBOX_CONTAINER_PASSW, String::class.java, null)
        for (operator in Authentication.OPERATORS) {
            val item = container.addItem(operator.id)
            item.getItemProperty(COMBOBOX_CONTAINER_FIO).value = operator.fio
            item.getItemProperty(COMBOBOX_CONTAINER_PASSW).value = operator.passw
        }
        return container
    }

    companion object {
        private const val serialVersionUID = 1L
        private const val COMBOBOX_CONTAINER_FIO = "fio"
        private const val COMBOBOX_CONTAINER_PASSW = "passw"
    }
}