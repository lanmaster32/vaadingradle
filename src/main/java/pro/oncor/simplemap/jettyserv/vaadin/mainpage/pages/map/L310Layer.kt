package pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.map

import org.vaadin.addon.leaflet.LTileLayer

class L310Layer : LTileLayer("https://maps.3101010.ru/osmbrightren/{z}/{x}/{y}.png") {
    init {
        attributionString = "Map data © <a href=\"https://openstreetmap.org\">OpenStreetMap</a> contributors"
    }
}