package pro.oncor.simplemap.jettyserv.vaadin.mainpage

import com.vaadin.annotations.Push
import com.vaadin.annotations.Theme
import com.vaadin.annotations.Title
import com.vaadin.annotations.VaadinServletConfiguration
import com.vaadin.server.VaadinRequest
import com.vaadin.server.VaadinServlet
import com.vaadin.ui.UI
import org.slf4j.LoggerFactory
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.authentication.Authentication
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.authenticatedwindow.AuthenticatedUserWindow
import pro.oncor.simplemap.jettyserv.vaadin.mainpage.pages.login.LoginWindow
import javax.servlet.annotation.WebServlet

@Title("Demo page title")
@Theme("tests-valo-dark")
@Push
class ReceiveOrderVaadinUI : UI() {
    @WebServlet(value = ["/*"], asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = ReceiveOrderVaadinUI::class)
    class Servlet : VaadinServlet()

    override fun init(request: VaadinRequest) {
        setSessionAttribute(SessionAttributes.AUTHENTICATION, Authentication())

        // Сначала нужно вывести форму логина
        // Если идентификатор пользователя отсутствует в сессиионных переменных, то выводим форму логина
        content = if (getSessionAttribute(SessionAttributes.COD_OP) == null) {
            LoginWindow()
        } else {
            AuthenticatedUserWindow()
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(ReceiveOrderVaadinUI::class.java)

        /**
         * Получение сессионного хранилища объектов
         */
        @Synchronized
        fun getSessionMapper(ui: UI): MutableMap<String?, Any?>? {
            var sam: HashMap<String?, Any?>? = null
            try {
                // Если хранилище объектов сессии еще не существует - создаем его
                if (ui.session.session.getAttribute(SessionAttributes.sam) == null) {
                    ui.session.session.setAttribute(SessionAttributes.sam, HashMap<String, Any>())
                }
                sam = ui.session.session.getAttribute(SessionAttributes.sam) as HashMap<String?, Any?>
            } catch (e: Exception) {
                logger.error("Ошибка", e)
            }
            return sam
        }

        /**
         * Установка сессионной переменной
         */
        @Synchronized
        fun setSessionAttribute(sessionAttributeName: String?, sessionAttributeValue: Any?) {
            val sessionMapper: MutableMap<String?, Any?>? = getSessionMapper(UI.getCurrent())
            sessionMapper!![sessionAttributeName] = sessionAttributeValue

        }

        /**
         * Получение значения сессионной переменной
         */
        @Synchronized
        fun getSessionAttribute(sessionAttributeName: String?): Any? {
            val sessionMapper: MutableMap<String?, Any?>? = getSessionMapper(UI.getCurrent())
            return sessionMapper!![sessionAttributeName];
        }

    }
}